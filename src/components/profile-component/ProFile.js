import React from 'react';
import ChangePassword from './ChangePassword';
import ChangeProfile from './changeProfile';
import { Redirect } from 'react-router-dom';
import './ProFile.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      user: [],
      posts: [],
      isLogin: localStorage.getItem("token") != null
    }
  }
  componentDidMount() {
    this.loadDataUser()
  }

  loadDataUser = () =>{
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("http://127.0.0.1:8000/api/users/me/", requestOptions)
      .then(response => {
        if (response.ok){
          return response.json()
        }
        throw new Error(response.status)
      })
      .then(result => {
        this.setState({ user: result, posts: result.posts});
        console.log(this.state.user)
      })                              
      .catch(error => {
        console.log('error', error)
      });
  }

  deletePost = (event) => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));

    var requestOptions = {
      method: 'DELETE',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("http://127.0.0.1:8000/api/post/" + event +"/", requestOptions)
      .then(response => response.text())
      .then(result => {
        this.loadDataUser();
      })
      .catch(error => console.log('error', error));
  }

  render() {
    if(!this.state.isLogin){
      return <Redirect to={"/login"} />
    }
    return (
      <div className="container profile-top">
      <div className="row">
        <div className="col-lg-4">
          <div className="profile-card-4 z-depth-3">
            <div className="card">
              <div className="card-body text-center bg-primary rounded-top">
                {this.state.user.avatar
                ?
                <div className="user-box">
                  <img src={this.state.user.avatar} alt="user avatar" />
                </div>
                :
                <div className="user-box">
                  <img src="https://e7.pngegg.com/pngimages/416/62/png-clipart-anonymous-person-login-google-account-computer-icons-user-activity-miscellaneous-computer.png" alt="user avatar" />
                </div>
                }
                <h5 className="mb-1 text-white">{this.state.user.name}</h5>
                <h6 className="text-light">UI/UX Engineer</h6>
              </div>
              <div className="card-body">
                <ul className="list-group shadow-none">
                  <li className="list-group-item">
                    <div className="list-icon">
                      <i className="fa fa-phone-square" />
                    </div>
                    <div className="list-details">
                      <span>{this.state.user.phone_number}</span>
                      <small>Mobile Number</small>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="list-icon">
                      <i className="fa fa-envelope" />
                    </div>
                    <div className="list-details">
                      <span>{this.state.user.email}</span>
                      <small>Email Address</small>
                    </div>
                  </li>
                  <li className="list-group-item">
                    <div className="list-icon">
                      <i className="fa fa-globe" />
                    </div>
                    <div className="list-details">
                      <span>www.example.com</span>
                      <small>Website Address</small>
                    </div>
                  </li>
                </ul>
                <div className="row text-center mt-4">
                  <div className="col p-2">
                    <h4 className="mb-1 line-height-5">154</h4>
                    <small className="mb-0 font-weight-bold">Projects</small>
                  </div>
                  <div className="col p-2">
                    <h4 className="mb-1 line-height-5">2.2k</h4>
                    <small className="mb-0 font-weight-bold">Followers</small>
                  </div>
                  <div className="col p-2">
                    <h4 className="mb-1 line-height-5">9.1k</h4>
                    <small className="mb-0 font-weight-bold">Views</small>
                  </div>
                </div>
              </div>
              <div className="card-footer text-center">
                <a href="/" className="btn-social btn-facebook waves-effect waves-light m-1"><i className="fa fa-facebook" /></a>
                <a href="/" className="btn-social btn-google-plus waves-effect waves-light m-1"><i className="fa fa-google-plus" /></a>
                <a href="/" className="list-inline-item btn-social btn-behance waves-effect waves-light"><i className="fa fa-behance" /></a>
                <a href="/" className="list-inline-item btn-social btn-dribbble waves-effect waves-light"><i className="fa fa-dribbble" /></a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-8">
          <div className="card z-depth-3">
            <div className="card-body">
              <ul className="nav nav-pills nav-pills-primary nav-justified">
                <li className="nav-item">
                  <a href="/" data-target="#profile" data-toggle="pill" className="nav-link active show"><i className="icon-user" /> <span className="hidden-xs">Profile</span></a>
                </li>
                <li className="nav-item">
                  <a href="/" data-target="#edit" data-toggle="pill" className="nav-link"><i className="icon-envelope-open" /> <span className="hidden-xs">Edit</span></a>
                </li>
                <li className="nav-item">
                  <a href="/" data-target="#edit-security" data-toggle="pill" className="nav-link"><i className="icon-note" /> <span className="hidden-xs">Edit security</span></a>
                </li>
              </ul>
              <div className="tab-content p-3">
                <div className="tab-pane active show" id="profile">
                  <h5 className="mb-3">User Profile</h5>
                  <div className="row">
                    <div className="col-md-6">
                      <h6>About</h6>
                      <p>
                        Web Designer, UI/UX Engineer
                      </p>
                      <h6>Hobbies</h6>
                      <p>
                        Indie music, skiing and hiking. I love the great outdoors.
                      </p>
                    </div>
                    <div className="col-md-6">
                      <span className="badge badge-primary"><i className="fa fa-user" /> 900 Followers</span>
                      <span className="badge badge-success"><i className="fa fa-cog" /> 43 Forks</span>
                      <span className="badge badge-danger"><i className="fa fa-eye" /> 245 Views</span>
                    </div>
                    <div className="col-md-12">
                      <h5 className="mt-2 mb-3"><span className="fa fa-clock-o ion-clock float-right" /> Recent Activity</h5>
                      <div className="box-body p-0">
                        {this.state.posts.map((post) =>(
                          <div className="p-3 d-flex align-items-center osahan-post-header">
                            <div className="font-weight-bold mr-3">
                            <div>
                                <span className="font-weight-normal">{post.content}</span> 
                                <div className="small text-success">{post.timestamp}</div>
                            </div>
                            </div>
                            <span className="ml-auto mb-auto">
                            <div className="btn-group">
                                <button type="button" className="btn btn-light btn-sm rounded" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <FontAwesomeIcon icon={faEllipsisV} />
                                </button>
                                <div className="dropdown-menu dropdown-menu-right">
                                <button className="dropdown-item" type="button" key={post.id} onClick={() => this.deletePost(post.id)}><i className="mdi mdi-delete" /> Delete</button>
                                <button className="dropdown-item" type="button"><i className="mdi mdi-close" /> Read</button>
                                </div>
                            </div>
                            </span>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                  {/*/row*/}
                </div>
                <ChangeProfile />
                <ChangePassword id={this.state.user.id}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

export default (Profile);