import React from 'react';
import './changeprofile.css';

class ChangePassword extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          old_password: "",
          new_password: "",
          confirm_password: "",
          error_change: false,
          success_change: null
        }
    }

    setParams = (event) => {
        this.setState({[event.target.name] : event.target.value })
    }

    handleChangePassword = (event) => {
        const id = this.props.id;
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));

        var formdata = new FormData();
        formdata.append("old_password", this.state.old_password);
        formdata.append("new_password", this.state.new_password);
        formdata.append("confirm_new_password", this.state.confirm_password);

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
        };

        fetch("http://127.0.0.1:8000/api/users/" + id + "/change-password/", requestOptions)
        .then(response => {
            if (response.ok){
                return response.json()
            }
            throw response.json()
        })
        .then(result => {
            this.setState({success_change: result});
        })
        .catch(error => {
            this.setState({error_change: true})
        });
    }

    render (){
        return (
            <div className="tab-pane" id="edit-security">
                {this.state.error_change
                ?
                <div className="alert alert-danger" role="alert">
                    Your current password is incorrect / New password and confirm new password must be the same
                </div>
                :
                <p></p>
                }
                {this.state.success_change
                ?
                <div className="alert alert-success" role="alert">
                    {this.state.error_change.old_password}
                </div>
                :
                <p></p>
                }
                <form>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label form-control-label">Old Password</label>
                    <div className="col-lg-9">
                    <input className="form-control" type="password" name="old_password" onChange={this.setParams} defaultValue={11111122333} />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label form-control-label">Password</label>
                    <div className="col-lg-9">
                    <input className="form-control" type="password" name="new_password" onChange={this.setParams} defaultValue={11111122333} />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label form-control-label">Confirm password</label>
                    <div className="col-lg-9">
                    <input className="form-control" type="password" name="confirm_password" onChange={this.setParams} defaultValue={11111122333} />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-lg-3 col-form-label form-control-label" />
                    <div className="col-lg-9">
                    <input type="reset" className="btn btn-secondary" defaultValue="Cancel" />
                    <button type="button" className="btn btn-primary" onClick={this.handleChangePassword}>Save Change</button>
                    </div>
                </div>
                </form>
            </div>
        )
    }
}

export default ChangePassword;