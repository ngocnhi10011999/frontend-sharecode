import React from 'react';
import { Link } from 'react-router-dom';
import './Post.css';

const size_page = 6;

class Post extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        posts: [],
        isLoad: false,
        isLogin: localStorage.getItem("token") != null,
        no_result: false,
        count_post: 0,
        pagination: [], 
        tag: this.props.tag,
    }
  }

  pagination = (event) =>{
    var number_page = this.state.count_post / size_page;
    var list_number_page = [];

    for(var i = 1; i <= number_page; i++){
      list_number_page.push(i)
    }

    if (this.state.count_post % size_page > 0){
      list_number_page.push(list_number_page.slice(-1)[0] + 1)
    }

    return list_number_page
  }

  loadDataPost = (event) => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    let text_search = ""
    if(this.props.search){
      text_search = "search=" + this.props.search;
    };
    if(event){
      text_search = event;
    };
    fetch("http://127.0.0.1:8000/api/post/?" + text_search, requestOptions)
    .then(response => {
        if (response.ok){
          return response.json()
        }
        throw new Error(response.status)
      })
      .then(result => {
        if(result.length === 0){
          this.setState({"no_result": true});
        }
        this.setState({ 
          posts: result.results,
          previous: result.previous,
          count_post: result.count,
          pagination: this.pagination()
        });
      })
      .catch(error => {console.log('error', error)});
  }
  change_page = () => {
    alert(this.state.previous)
  }

  componentDidMount() {
    this.loadDataPost()
  }

  render() {
    // var list_need = this.pagination();
    // const list_page = list_need.map((number) => (
    //   <li key={number.toString()} className="page-item"><Link className="page-link" onClick={this.loadDataPost} to={`/posts?page=${number}`}>{number}</Link></li>
    // ))
    return (
      <div className="container padding-top">
          <div id="portfolio">
          <div className="container-flux">
            <div className="row no-gutters">
              <div className="col-lg-6"><Link className="portfolio-item" to={"/"}>
                  <div className="caption">
                    <div className="caption-content">
                      <h2>Strawberries</h2>
                      <p className="mb-0">Strawberries are such a tasty snack, especially with a little sugar on top!</p>
                    </div>
                  </div><img className="img-fluid" alt="description" src="assets/img/portfolio-3.jpg" />
                </Link></div>
              <div className="col-lg-6"><Link className="portfolio-item" to={"/"}>
                  <div className="caption">
                    <div className="caption-content">
                      <h2>Workspace</h2>
                      <p className="mb-0">A yellow workspace with some scissors, pencils, and other objects.</p>
                    </div>
                  </div><img className="img-fluid" alt="description" src="assets/img/portfolio-4.jpg" />
                </Link></div>
            </div>
          </div>
        </div>
        <section className="content inbox">
          <div className="container-fluid">     
            <div className="row clearfix">
            {this.state.no_result
            ?
              <div className="alert alert-danger" role="alert">
                  No has posts !
              </div>
            :
              <div className="col-md-12 col-lg-12 col-xl-12">
                <ul className="mail_list list-group list-unstyled">
                  {this.state.posts.map(post => (
                  <li className="list-group-item">
                    <div className="media">
                      <div className="pull-left">                                
                        <div className="thumb hidden-sm-down m-r-20"> <img src="assets/images/xs/avatar1.jpg" className="rounded-circle" alt="" /> </div>
                      </div>
                      <div className="media-body">
                        <div className="media-heading">
                          <Link to={`posts/${post.id}`} className="m-r-10">{post.user}&nbsp;&nbsp;</Link>
                          {post.tags.map((tag) => (
                            <Link className="badge badge-info" key={tag} to={`/posts?tag=${tag}`} onClick={() => this.loadDataPost("tags__name=" + tag)}>{tag}</Link>
                            ))
                          }
                          <small className="float-right text-muted"><time className="hidden-sm-down" dateTime={2021}>{post.timestamp}</time><i className="zmdi zmdi-attachment-alt" /> </small>
                        </div>
                        <p className="msg">{post.content}</p>
                      </div>
                    </div>
                  </li>
                  ))}
                </ul>
                <div className="card m-t-5">
                  <div className="body">
                    <ul className="pagination pagination-primary m-b-0">
                    <li className="page-item"><Link className="page-link" to={`/posts?previous_page`} onClick={this.change_page} >Previous</Link></li>
                      {/* {list_page} */}
                      <li className="page-item"><Link className="page-link" to={`/posts?next_page`}>Next</Link></li>
                    </ul>
                    {/* <ul className="pagination pagination-primary m-b-0">
                      <li className="page-item active"><Link className="page-link" to="#">1</Link></li>
                      <li className="page-item"><Link className="page-link" to="#">2</Link></li>
                      <li className="page-item"><Link className="page-link" to="#">3</Link></li>
                      <li className="page-item"><Link className="page-link" to="#">Next</Link></li>
                    </ul> */}
                  </div>
                </div>
              </div>
            }
            </div>
          </div>
        </section>
      </div>
    );
    }
}

export default Post;
