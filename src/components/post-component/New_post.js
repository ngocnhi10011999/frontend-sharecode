import React from 'react';

class NewPost extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          "content": "",
          "description": "",
          "tags": [],
          "attachment": null,
          "image1": null,
          "image2": null,
          "image3": null,
          "success": false
        }
    }

    handleFileChange = (e) => {
      console.log(e.target.files);
      this.setState({
        "attachment": e.target.files[0],
        "image1": e.target.files[0],
        "image2": e.target.files[0],
        "image3": e.target.files[0]
      })
    };

    setParams = (event) => {
    this.setState({[event.target.name] : event.target.value })
    }

    submitPost = (event) => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
    
        var formdata = new FormData();
        formdata.append("content", this.state.content);
        formdata.append("description", this.state.description);
        formdata.append("tags", ["python"]);
        formdata.append("attachment", this.state.attachment, this.state.attachment.name);
        formdata.append("image1", this.state.image1, this.state.image1.name);
        formdata.append("image2", this.state.image2, this.state.image2.name);
        formdata.append("image3", this.state.image3, this.state.image3.name);
    
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
        };
        fetch("http://127.0.0.1:8000/api/post/", requestOptions)
          .then(response => {
            if (response.ok){
              return response.json()
            }
            throw Error(response.status)
          })
          .then(result => {
            this.setState({"success": true})
          })
          .catch(error => {
            console.log('error', error)
          });
    }
    
    render() {
      return (
        <div className="new-post">
          {this.state.success
          ?
          <div className="alert alert-success" role="alert">
            A new post success!
          </div>
          :
          <p></p>
          }
          <form>
              <div className="mb-3">
                  <input type="text" className="form-control" id="contentInput" name="content" onChange={this.setParams} placeholder="Title" />
              </div>
              <div className="mb-3">
                  <input type="text" className="form-control" id="tagsInput" name="tags" onChange={this.setParams} placeholder="Tags" />
              </div>
              <div className="form-group">
                  <textarea className="form-control" id="descriptionInput" name="description" rows={7} defaultValue={""} onChange={this.setParams} placeholder="Description" />
              </div>
              <div className="form-group">
                  <input type="file" className="form-control-file" id="attachmentInput" onChange={this.handleFileChange} />
              </div>
              <p>Select 3 image</p>
              <div className="form-group">
                  <input type="file" className="form-control-file" id="attachmentInput" onChange={this.handleFileChange} />
              </div>
              <div className="form-group">
                  <input type="file" className="form-control-file" id="attachmentInput" onChange={this.handleFileChange} />
              </div>
              <div className="form-group">
                  <input type="file" className="form-control-file" id="attachmentInput" onChange={this.handleFileChange} />
              </div>
              <button type="button" className="btn btn-primary" onClick={this.submitPost} >Post</button>
          </form>
        </div>
      )
    }
  }

export default NewPost;