import React from 'react';
import './Post.css';
import { Link, Redirect, withRouter } from "react-router-dom";
import { faCaretDown, faCaretUp, faComment, faPaperclip, faDownload, faEye } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class PostDetail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        post: [],
        comment: [],
        isLoad: false,
        sendcomment: "",
        isLogin: localStorage.getItem("token") != null,
        vote: true,
        commentError: false,
        posts_trend: []
    }
  }

  setParams = (event) => {
    this.setState({[event.target.name] : event.target.value })
  }

  componentDidMount(){
    this.loadData()
  }

  loadDataTrend(){
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://127.0.0.1:8000/api/post/post-trending/", requestOptions)
    .then(response => {
        if (response.ok){
          return response.json()
        }
        throw new Error(response.status)
      })
      .then(result => {
        this.setState({ 
          posts_trend: result,
        });
      })
      .catch(error => {console.log('error', error)});
  }

  loadData(){
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    let id = this.props.match.params.id;
    fetch("http://127.0.0.1:8000/api/post/" + id + "/", requestOptions)
    .then(response => {
      if (response.ok){
        return response.json()
      }
      throw new Error(response.status)
    })
    .then(result => {
      this.setState({ 
        post: result,
        comment: result.comment
      })
      console.log('result:', this.props.state.post)
    })
    .catch(error => console.log('error', error));
  }

  handleSendComment = (event) => {
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));

    var formdata = new FormData();
    formdata.append("title", this.state.sendcomment);

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: formdata,
      redirect: 'follow'
    };
    let id = this.props.match.params.id;
    fetch("http://127.0.0.1:8000/api/post/" + id + "/comment-post/", requestOptions)
      .then(response => {
        if (response.ok){
          return response.json()
        }
        throw Error(response.status)
      })
      .then(result => {
        this.loadData()
      })
      .catch(error => {
        this.setState({commentError: true})
        console.log('error', error)
      });
  }

  handleVotes = (event) =>{
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"))

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      redirect: 'follow'
    };
    let id = this.props.match.params.id;
    fetch("http://127.0.0.1:8000/api/post/" + id + "/mark-positive/", requestOptions)
      .then(response => {
        if (response.ok){
          this.loadData()
        }
        throw Error(response.status)
      })
      .then(result => {console.log(result)})
      .catch(error => {this.setState({vote: false})});
  }

  handleUnVotes = (event) =>{
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"))

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      redirect: 'follow'
    };
    let id = this.props.match.params.id;
    fetch("http://127.0.0.1:8000/api/post/" + id + "/mark-negative/", requestOptions)
      .then(response => {
        if (response.ok){
          this.loadData()
        }
        throw Error(response.status)
      })
      .then(result => {this.loadData()})
      .catch(error => {this.setState({vote: false})});
  }

  attachmentPost = (event) =>{
    window.open(event);
  }

  render() {
    if(this.state.vote === false){
      <Redirect to="/login"/>
    }
    return (
      <div className="container pb50 padding-top">
        <div className="row">
          <div className="col-md-1 mb40">
            <div className="actions-top">
              <div className="votes votes--side post-actions__vote">
                <button className="btn-vote" onClick={this.handleVotes}><FontAwesomeIcon icon={faCaretUp}/></button>
                <div className="text-muted">
                  {this.state.post.vote}
                </div>
                <button className="btn-vote" data-tippy data-original-title="Negative" onClick={this.handleUnVotes}><FontAwesomeIcon icon={faCaretDown}/></button>
              </div>
              <div className="votes--side post-actions__vote text-muted">
                <FontAwesomeIcon icon={faComment}/><span className="text-comment">{this.state.comment.length}</span>
              </div>
              <div className="votes--side">
                <button className="post-actions__clip">
                  <FontAwesomeIcon icon={faPaperclip}/>
                </button>
              </div>
              <div className="votes--side">
                <button className="post-actions__clip">
                  <FontAwesomeIcon icon={faEye}/>
                </button>
                <div className="text-muted">
                  {this.state.post.views}
                </div>
              </div>
              <div className="votes--side">
                <button className="post-actions__clip" key={this.state.post.attachment} onClick={() => this.attachmentPost(this.state.post.attachment)}>
                  <FontAwesomeIcon icon={faDownload}/>
                </button>
                <div className="text-muted">
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-8 mb40">
            <article>
              {/* <img src="https://images.unsplash.com/photo-1455734729978-db1ae4f687fc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80" alt="" className="img-fluid mb30" /> */}
              <div className="img-fluid mb30">
                <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                  <div className="carousel-inner">
                    <div className="carousel-item active">
                      <img className="d-block w-100" src={this.state.post.image1} alt="First slide" />
                    </div>
                    <div className="carousel-item">
                      <img className="d-block w-100" src={this.state.post.image1} alt="Second slide" />
                    </div>
                    <div className="carousel-item">
                      <img className="d-block w-100" src={this.state.post.image1} alt="Third slide" />
                    </div>
                  </div>
                  <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true" />
                    <span className="sr-only">Previous</span>
                  </a>
                  <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true" />
                    <span className="sr-only">Next</span>
                  </a>
                </div>
              </div>
              <div className="post-content">
                <h3> {this.state.post.content}</h3>
                <ul className="post-meta list-inline">
                  <li className="list-inline-item">
                    <i className="fa fa-user-circle-o" /> <Link to="/userdetail">{this.state.post.user}</Link>
                  </li>
                  <li className="list-inline-item">
                    <i className="fa fa-calendar-o" /> <a href="/">{this.state.post.timestamp}</a>
                  </li>
                  <li className="list-inline-item">
                    <i className="fa fa-tags" /> <a href="/">{this.state.post.tags}</a>
                  </li>
                </ul>
                <div className="lead">
                  {this.state.post.description}
                </div>
                <ul className="list-inline share-buttons">
                <li className="list-inline-item">Share Post:</li>
                  <li className="list-inline-item">
                    <a href="/" className="social-icon-sm si-dark si-colored-facebook si-gray-round">
                      <i className="fa fa-facebook" />
                      <i className="fa fa-facebook" />
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a href="/" className="social-icon-sm si-dark si-colored-twitter si-gray-round">
                      <i className="fa fa-twitter" />
                      <i className="fa fa-twitter" />
                    </a>
                  </li>
                  <li className="list-inline-item">
                    <a href="/" className="social-icon-sm si-dark si-colored-linkedin si-gray-round">
                      <i className="fa fa-linkedin" />
                      <i className="fa fa-linkedin" />
                    </a>
                  </li>
                </ul>
                <hr className="mb40" />
                <h4 className="mb40 text-uppercase font500">Comments</h4>
                {this.state.comment
                  ? <p></p>
                  : <p>There is no comment</p>
                }
                {this.state.comment.map(cmt =>(
                <div className="media mb40">
                  <i className="d-flex mr-3 fa fa-user-circle-o fa-3x" />
                  <div className="media-body">
                    <h5 className="mt-0 font400 clearfix">
                      <a href="/" className="float-right">Remove</a>
                    {cmt.user}</h5>{cmt.title}
                  </div>
                </div>))}
                <hr className="mb40" />
                <h4 className="mb40 text-uppercase font500">Post a comment</h4>
                <form>
                  {this.state.commentError
                  ?
                  <div className="alert alert-danger" role="alert">
                    Invalid comment
                  </div>
                  :
                  <p></p>
                  }  
                  <div className="form-group">
                    <label>Comment</label>
                    <textarea className="form-control" rows={5} placeholder="Comment" name="sendcomment" onChange={this.setParams} defaultValue={""} />
                  </div>
                  <div className="clearfix float-right">
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.handleSendComment} >Send</button>
                  </div>
                </form>
              </div>
            </article>
            {/* post article*/}
          </div>
          <div className="col-md-3 mb40">
            <div>
              <h4 className="sidebar-title">Trendding</h4>
              <ul className="list-unstyled">
                {this.state.posts_trend.map((post) => (
                  <li className="media">
                    <div className="media-body">
                      <h5 className="mt-0 mb-1"><a href="/">{post.content}</a></h5>{post.timestamp}
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
   }
}
export default withRouter(PostDetail);