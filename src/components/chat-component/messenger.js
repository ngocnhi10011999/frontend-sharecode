import React from 'react';
import { Link } from 'react-router-dom';
import './chat.css';

class Messenger extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            messenges: [],
            isLoad: false,
            isLogin: localStorage.getItem("token") != null,
            user: null,
            reply: ""
        }
      }

    componentDidMount(){
        this.loadMessenges();
        this.loadDataUser();
    }

    setParams = (event) => {
        this.setState({[event.target.name] : event.target.value })
    }
    
    loadDataUser(){
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
        var requestOptions = {
          method: 'GET',
          headers: myHeaders,
          redirect: 'follow'
        };
        fetch("http://127.0.0.1:8000/api/users/me/", requestOptions)
          .then(response => {
            if (response.ok){
              return response.json()
            }
            throw new Error(response.status)
          })
          .then(result => {this.setState({ user: result.id})})
          .catch(error => {console.log('error', error)});
    }

    loadMessenges = (event) =>{
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"))

        var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
        };

        fetch("http://127.0.0.1:8000/api/message/", requestOptions)
        .then(response => {
            if(response.ok){
                return response.json()
            }
            throw Error(response.status)
        })
        .then(result => {
            this.setState({"messenges": result.results})
        })
        .catch(error => console.log('error', error));
    }

    handleSubmit = (event) => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
        
        var formdata = new FormData();
        formdata.append("content", this.state.reply);
        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
        };
        document.getElementById("reply-form").reset();
        fetch("http://127.0.0.1:8000/api/message/reply/", requestOptions)
        .then(response => response.text())
        .then(result => {this.loadMessenges()})
        .catch(error => console.log('error', error));
    }

    render(){
        return(
            <div className="padding-top">
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
                <div className="container">
                <div className="row clearfix">
                    <div className="col-lg-12">
                        <div className="card chat-app">
                            <div id="plist" className="people-list">
                            <div className="input-group">
                                <div className="input-group-prepend">
                                <span className="input-group-text"><i className="fa fa-search" /></span>
                                </div>
                                <input type="text" className="form-control" placeholder="Search..." />
                            </div>
                            <ul className="list-unstyled chat-list mt-2 mb-0">
                                <li className="clearfix">
                                <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="avatar" />
                                <div className="about">
                                    <div className="name">Vincent Porter</div>
                                    <div className="status"> <i className="fa fa-circle offline" /> left 7 mins ago </div>                                            
                                </div>
                                </li>
                            </ul>
                            </div>
                            <div className="chat">
                            <div className="chat-header clearfix">
                                <div className="row">
                                <div className="col-lg-6">
                                    <Link href="" data-toggle="modal" data-target="#view_info">
                                    <img src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="avatar" />
                                    </Link>
                                    <div className="chat-about">
                                    <h6 className="m-b-0">Aiden Chavez</h6>
                                    <small>Last seen: 2 hours ago</small>
                                    </div>
                                </div>
                                <div className="col-lg-6 hidden-sm text-right">
                                    <Link href="" className="btn btn-outline-secondary"><i className="fa fa-camera" /></Link>
                                    <Link href="" className="btn btn-outline-primary"><i className="fa fa-image" /></Link>
                                    <Link href="" className="btn btn-outline-info"><i className="fa fa-cogs" /></Link>
                                    <Link href="" className="btn btn-outline-warning"><i className="fa fa-question" /></Link>
                                </div>
                                </div>
                            </div>
                            <div className="chat-history">
                                <ul className="m-b-0">
                                    {this.state.messenges.map(mess => (
                                        mess.id_user === this.state.user
                                        ?
                                        <li className="clearfix">
                                            <div className="message-data text-right">
                                            <span className="message-data-time">{mess.timestamp}</span>
                                            <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" />
                                            </div>
                                            <div className="message other-message float-right">{mess.content}</div>
                                        </li>
                                        :
                                        <li className="clearfix">
                                            <div className="message-data">
                                            <span className="message-data-time">{mess.timestamp}</span>
                                            </div>
                                        <div className="message my-message">{mess.content}</div>                                    
                                        </li>
                                                
                                    ))}    
                                </ul>
                            </div>
                            <div className="chat-message clearfix">
                                <form id="reply-form">
                                    <div className="input-group mb-0">
                                        <div className="input-group-prepend">
                                            <button type="button" className="input-group-text" onClick={this.handleSubmit}><i className="fa fa-send" /></button>
                                        </div>
                                        <input type="text" className="form-control" name="reply" onChange={this.setParams} placeholder="Enter text here..." />                                 
                                    </div>
                                </form>   
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

export default Messenger;