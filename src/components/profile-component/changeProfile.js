import React from 'react';
import './ProFile.css';

class ChangeProfile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          user: [],
          isLogin: localStorage.getItem("token") != null,
          name: null,
          email: null,
          avatar: null,
          phone_number: null,
          company: null,
          update_profile: null
        }
    }

    setParams = (event) => {
      this.setState({[event.target.name] : event.target.value })
    }

    componentDidMount() {
        this.loadDataUser()
    }
    
    loadDataUser(){
      var myHeaders = new Headers();
      myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
      var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch("http://127.0.0.1:8000/api/users/me/", requestOptions)
        .then(response => {
          if (response.ok){
            return response.json()
          }
          throw new Error(response.status)
        })
        .then(result => {this.setState({ user: result})})
        .catch(error => {console.log('error', error)});
    }

    handleFileChange = (e) => {
      this.setState({
        "avatar": e.target.files[0]
      })
    };
    
    handleChangeProfile = (event) =>{
      var myHeaders = new Headers();
      myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"))

      var formdata = new FormData();
      formdata.append("name", this.state.name);
      formdata.append("email", this.state.email);
      formdata.append("company ", this.state.company);

      var requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
      };

      fetch("http://127.0.0.1:8000/api/users/" + this.state.user.id + "/  ", requestOptions)
        .then(response => {
          if (response.ok){
            return response.json()
          }
          throw new Error(response.status)
        })
        .then(result => {
          this.setState({update_profile: true, result})
        })
        .catch(error => {
          this.setState({update_profile: false})
        });
    }

    changeAvatar = (event) =>{
      var myHeaders = new Headers();
      myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"))

      var formdata = new FormData();
      formdata.append("avatar", this.state.avatar, this.state.avatar.name);
      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: formdata,
        redirect: 'follow'
      };

      fetch("http://127.0.0.1:8000/api/users/" + this.state.user.id + "/change-avatar/", requestOptions)
        .then(response => {
          if(response.ok){
            return response.json()
          }})
        .then(result => this.loadDataUser())
        .catch(error => console.log('error', error));
    }

    render (){
        return (
            <div className="tab-pane" id="edit">
              {this.state.update_profile === false
                ?
                <div className="alert alert-danger" role="alert">
                    Invalid information !
                </div>
                :
                <p></p>
                }
                {this.state.update_profile === true
                ?
                <div className="alert alert-success" role="alert">
                    Update Successfully !
                </div>
                :
                <p></p>
                }
                <form>
                 <div className="form-group avatar">
                    <figure className="figure col-md-2 col-sm-3 col-xs-12">
                      <img className="img-rounded img-responsive" src={this.state.user.avatar} alt="" />
                    </figure>
                    <div className="form-inline col-md-10 col-sm-9 col-xs-12">
                      <input type="file" className="file-uploader pull-left" onChange={this.handleFileChange} required/>
                      <button type="button" className="btn btn-sm btn-default-alt pull-left" onClick={this.changeAvatar}>Update Image</button>
                    </div>
                  </div>
                </form>
                <form>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label form-control-label">Name</label>
                  <div className="col-lg-9">
                    <input className="form-control" type="text" name="name" onChange={this.setParams} />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label form-control-label">Email</label>
                  <div className="col-lg-9">
                    <input className="form-control" type="email" name="email" onChange={this.setParams} />
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label form-control-label">Company</label>
                  <div className="col-lg-9">
                    <input className="form-control" name="company" type="text" onChange={this.setParams}/>
                  </div>
                </div>
                <div className="form-group row">
                  <label className="col-lg-3 col-form-label form-control-label" />
                  <div className="col-lg-9">
                    <input type="reset" className="btn btn-secondary" defaultValue="Cancel" />
                    <input type="button" className="btn btn-primary" onClick={this.handleChangeProfile} defaultValue="Save Changes" />
                  </div>
                </div>
              </form> 
            </div>
                
        )
    }
}

export default ChangeProfile;