import React from 'react'
import { BrowserRouter as Router, Link } from 'react-router-dom';
import './footer.css'

class Footer extends React.Component {
  render() {
    return (<Router>
        <footer className="footer text-center bg-footer">
        <div className="container">
          <ul className="list-inline mb-5">
            <li className="list-inline-item">&nbsp;<Link className="text-white social-link rounded-circle" to="/"><i className="icon-social-facebook" /></Link></li>
            <li className="list-inline-item">&nbsp;<Link className="text-white social-link rounded-circle" to="/"><i className="icon-social-twitter" /></Link></li>
            <li className="list-inline-item">&nbsp;<Link className="text-white social-link rounded-circle" to="/"><i className="icon-social-github" /></Link></li>
          </ul>
          <p className="text-muted mb-0 small">Copyright &nbsp;© Brand 2021</p>
        </div><Link className="js-scroll-trigger scroll-to-top rounded" to="/"><i className="fa fa-angle-up" /></Link>
      </footer>
      </Router>
      );
  }
}
export default Footer;