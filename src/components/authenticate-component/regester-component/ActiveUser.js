import React from "react";
import { useParams } from "react-router-dom";

function ActiveUser({ token }) {
  let { uid } = useParams();

  function loadData(){
    var formdata = new FormData();
    formdata.append("uid", uid);
    formdata.append("token", token);
    
    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
    };
    
    fetch("http://127.0.0.1:8000/api/users/active/", requestOptions)
      .then(response => {
        if(response.ok){
          return response.json()
        }
        throw Error(response.status)
      })
      .then(result => {
        window.location.replace('/')
      })
      .catch(error => console.error(error));
  };
  window.onload = loadData();
  return(
  <div><p>Loading....</p></div>
  )
}


export default ActiveUser;