import './header.css';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Dropdown, Image, Form, FormControl, Button, Nav, Navbar } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faBell, faComment, faSearch } from '@fortawesome/free-solid-svg-icons'


class Header extends Component { 

  constructor(props) {
    super(props)
    this.state = {
      user: [],
      isLoad: false,
      isLogin: localStorage.getItem("token") != null,
      text_search: "",
      action_search: false
    }
  }

  setParams = (event) => {
    this.setState({[event.target.name] : event.target.value })
  }
  
  componentDidMount() {
    this.loadDataUser()
  }

  loadDataUser(){
    var myHeaders = new Headers();
    myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("http://127.0.0.1:8000/api/users/me/", requestOptions)
      .then(response => {
        if (response.ok){
          return response.json()
        }
        throw new Error(response.status)
      })
      .then(result => {
        this.setState({ user: result})
      })
      .catch(error => {
        console.log('error', error)
      });
  }
  
  handleSubmitLogOut = () => {
    localStorage.removeItem("token");
    this.loadDataUser();
    window.location.reload(false);
  }

  handleSubmitSearch = () => {
    window.location.replace("/posts?search=" + this.state.text_search);
  }

  render () {
    return (
      <div className="App">
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top">
          <Navbar.Brand>
            <Link className="navbar-brand" to="/">
              <h2>LOGO</h2>
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link>
                <Link className="nav-link" to="/">POST</Link>
              </Nav.Link>
              <Nav.Link>
                <Link className="nav-link" to="/">ABOUT</Link>
              </Nav.Link>
              <Form inline>
                <FormControl type="text" placeholder="Search" name="text_search" onChange={this.setParams} className="mr-sm-2" />
                <Button variant="outline-success" onClick={this.handleSubmitSearch}><FontAwesomeIcon icon={faSearch}/></Button>
              </Form>
            </Nav>
            {this.state.isLogin
            ?
            <Dropdown>
              <Dropdown.Toggle variant="Secondary" id="dropdown-basic">
                <Image className="logo-user-header" src={this.state.user.avatar} rounded />
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item><Link className="nav-link" to="/profile">ProFile</Link></Dropdown.Item>
                <Dropdown.Item><Link className="nav-link" to="/new-post">New Post</Link></Dropdown.Item>
                <Dropdown.Item><Button variant="outline-success" onClick={this.handleSubmitLogOut}>Log Out</Button></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            :
            <Dropdown>
              <Dropdown.Toggle variant="Secondary" id="dropdown-basic">
                <FontAwesomeIcon icon={faUser}/>
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item><Link className="nav-link" to="/login">Login</Link></Dropdown.Item>
                <Dropdown.Item><Link className="nav-link" to="/singup">SingUp</Link></Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            }
            <Nav>
              <Nav.Link><Link className="nav-link" to="/notifications"><FontAwesomeIcon icon={faBell}/></Link></Nav.Link>
              <Nav.Link><Link className="nav-link" to="/chatbox"><FontAwesomeIcon icon={faComment}/></Link></Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    )
  }
}
export default Header;
