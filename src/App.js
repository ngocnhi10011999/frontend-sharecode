import './App.css';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import LoginForm from './components/authenticate-component/login-component/LoginForm';
import SignupForm from './components/authenticate-component/regester-component/SignUpForm';
import Header from './components/header-component/header';
import Footer from './components/footer-component/footer';
import  Profile from './components/profile-component/ProFile';
import Post from './components/post-component/Post';
import NewPost from './components/post-component/New_post';
import PostDetail from './components/post-component/Post_detail';
import ActiveUser from './components/authenticate-component/regester-component/ActiveUser';
import UserDetail from './components/userDetail-component/UserDetail';
import Messenger from './components/chat-component/messenger';
import Noti from './components/noti-component/Noti';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function App (){

  let query = useQuery();

  return(
    <>
      <Header />
      <Switch>
        <Route path="/posts/:id">
          <PostDetail />
        </Route>
        <Route path="/active/:uid">
          <ActiveUser token={query.get("token")}/>
        </Route>
        <Route path="/posts">
          <Post search={query.get("search")} tag={query.get("tag") }/>
        </Route>
        <Route path="/new-post">
          <NewPost />
        </Route>
        <Route path="/login">
          <LoginForm />
        </Route>
        <Route path="/singup">
          <SignupForm />
        </Route>
        <Route path="/profile">
          <Profile />
        </Route>
        <Route path="/userdetail">
          <UserDetail />
        </Route>
        <Route path="/chatbox">
          <Messenger />
        </Route>
        <Route path="/notifications">
          <Noti />
        </Route>
        <Route path="/">
          <Redirect to="/posts" />
        </Route>
      </Switch>
      <Footer />
    </>

)};
export default App;
