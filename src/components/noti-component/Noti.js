import React from 'react';
import './Noti.css'
// import { Link } from 'react-router-dom';


class Noti extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          notis: [],
          isLoad: false,
          isLogin: localStorage.getItem("token") != null,
          text_search: "",
          action_search: false
        }
    }

    componentDidMount(){
        this.loadData()
    }

    loadData() {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "JWT " + localStorage.getItem("token"));

        var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
        };

        fetch("http://127.0.0.1:8000/api/notification/", requestOptions)
        .then(response => {
            if(response.ok){
                return response.json()
            }
            throw Error(response.status)
        })
        .then(result => {
            this.setState({"notis": result.results})
        })
        .catch(error => console.log('error', error));
    }
    render(){
        return (
            <div>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.3.45/css/materialdesignicons.css" integrity="sha256-NAxhqDvtY0l4xn+YVa6WjAcmd94NNfttjNsDmNatFVc=" crossOrigin="anonymous" />
                <div className="container">
                    <div className="box shadow-sm rounded bg-white mb-4 padding-top">
                        <div className="box-title border-bottom p-3">
                        <h6 className="m-0">Notifications</h6>
                        </div>
                        {this.state.notis.map((noti) => (
                            <div className="box-body p-0">
                                <div className="p-3 d-flex align-items-center osahan-post-header">
                                    <div className="dropdown-list-image mr-3">
                                    <img className="rounded-circle" src="https://bootdey.com/img/Content/avatar/avatar2.png" alt="" />
                                    </div>
                                    <div className="font-weight-bold mr-3">
                                    <div>
                                        <span className="font-weight-normal">{noti.title}</span> 
                                        <div className="small text-success"><i className="fa fa-check-circle" />{noti.content}</div>
                                    </div>
                                    </div>
                                    <span className="ml-auto mb-auto">
                                    <div className="btn-group">
                                        <button type="button" className="btn btn-light btn-sm rounded" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="mdi mdi-dots-vertical" />
                                        </button>
                                        <div className="dropdown-menu dropdown-menu-right">
                                        <button className="dropdown-item" type="button"><i className="mdi mdi-delete" /> Delete</button>
                                        <button className="dropdown-item" type="button"><i className="mdi mdi-close" /> Read</button>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="text-right text-muted pt-1">{noti.generated_at}</div>
                                    </span>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}
export default Noti;