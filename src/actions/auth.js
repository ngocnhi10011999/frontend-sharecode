export const userLoginFetch = user => {
  return dispatch => {
    return fetch("http://127.0.0.1:8000/api/auth/get-token/", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({user})
    })
      .then(resp => resp.json())
      .then(data => {
        if (data.message) {
        } else {
          localStorage.setItem("token", data.jwt)
          // dispatch(loginUser(data.user))
        }
      })
  }
}