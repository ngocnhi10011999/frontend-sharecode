import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import './LoginForm.css';

class LoginForm extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      "username": "",
      "password": "",
      "isLogin": false,
      "error_login": false
    }
  }

  setParams = (event) => {
    this.setState({[event.target.name] : event.target.value })
  }

  handleSubmit = (event) => {
    var formdata = new FormData();
    formdata.append("password", this.state.password);
    formdata.append("username", this.state.username);

    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
    };

    fetch("http://127.0.0.1:8000/api/auth/get-token/", requestOptions)
      .then(response => {
        if (response.ok){
          return response.json()
        }
        throw Error(response.status)
      })
      .then(result => {
        localStorage.setItem("token", result.token)
        this.setState({"isLogin": true});
        window.location.reload(false);
      })
      .catch(error => {
        this.setState({"error_login": true})
      });
  }

  render() {
    if(this.state.isLogin){
      return <Redirect to={"/"} />
    }
    return (
      <div className="login-form">
        {this.state.error_login
            ?
            <div className="alert alert-danger" role="alert">
                Invalid username or password !
            </div>
            :
            <p></p>
        }
        <form>
          <h2 className="text-center">Log in</h2>       
          <div className="form-group">
            <input type="email" className="form-control" name="username" onChange={this.setParams} placeholder="Email" required="required" />
          </div>
          <div className="form-group">
            <input type="password" className="form-control" name="password" onChange={this.setParams} placeholder="Password" required="required" />
          </div>
          <div className="form-group">
            <button type="button" className="btn btn-primary btn-block" onClick={this.handleSubmit}>Log in</button>
          </div>
          <div className="clearfix">
            <label className="float-left form-check-label"><input type="checkbox" /> Remember me</label>
            <Link className="float-right" to="/">Forgot Password?</Link>
          </div>        
        </form>
        <p className="text-center"><Link to="/signup">Create an Account</Link></p>
      </div>
    );
  }
}

export default (LoginForm);