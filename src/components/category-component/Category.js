import React from 'react';
import { Link } from 'react-router-dom';
// import PropTypes from 'prop-types';
import './Category.css'

class Category extends React.Component {

  render() {
    return (
        <section id="portfolio" className="content-section">
        <div className="container">
          <div className="content-section-heading text-center">
            <h3 className="text-secondary mb-0">Portfolio</h3>
            <h2 className="mb-5">Recent Projects</h2>
          </div>
          <div className="row no-gutters">
            <div className="col-lg-6"><Link className="portfolio-item" to={"/"}>
                <div className="caption">
                  <div className="caption-content">
                    <h2>Stationary</h2>
                    <p className="mb-0">A yellow pencil with envelopes on a clean, blue backdrop!</p>
                  </div>
                </div><img className="img-fluid" alt="description" src="assets/img/portfolio-1.jpg" />
              </Link></div>
            <div className="col-lg-6"><Link className="portfolio-item" to={"/"}>
                <div className="caption">
                  <div className="caption-content">
                    <h2>Ice Cream</h2>
                    <p className="mb-0">A dark blue background with a colored pencil, a clip, and a tiny ice cream cone!</p>
                  </div>
                </div><img className="img-fluid" alt="description" src="assets/img/portfolio-2.jpg" />
              </Link></div>
            <div className="col-lg-6"><Link className="portfolio-item" to={"/"}>
                <div className="caption">
                  <div className="caption-content">
                    <h2>Strawberries</h2>
                    <p className="mb-0">Strawberries are such a tasty snack, especially with a little sugar on top!</p>
                  </div>
                </div><img className="img-fluid" alt="description" src="assets/img/portfolio-3.jpg" />
              </Link></div>
            <div className="col-lg-6"><Link className="portfolio-item" to={"/"}>
                <div className="caption">
                  <div className="caption-content">
                    <h2>Workspace</h2>
                    <p className="mb-0">A yellow workspace with some scissors, pencils, and other objects.</p>
                  </div>
                </div><img className="img-fluid" alt="description" src="assets/img/portfolio-4.jpg" />
              </Link></div>
          </div>
        </div>
      </section>

    );
  }
}

export default Category;