import React from 'react';
// import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Regester.css'

class SignupForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      "username": "",
      "email": "",
      "password": "",
      "confirm_password": "",
      "createSuccess": false,
      "createFail": false
    }
  }

  setParams = (event) => {
    this.setState({[event.target.name] : event.target.value })
  }

  handleSubmit = (event) => {
    var formdata = new FormData();
    formdata.append("name", this.state.username);
    formdata.append("email", this.state.email);
    formdata.append("password", this.state.password);
    formdata.append("confirm_password", this.state.confirm_password);

    var requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
    };

    fetch("http://127.0.0.1:8000/api/users/", requestOptions)
    .then(response => {
      if (response.ok){
        return response.json()
      }
      throw response.json()
    })
    .then(result => {this.setState({"createSuccess": true, "createFail": false})})
    .catch(error => {
      this.setState({"createFail": true, "createSuccess": false});
      console.error('error', error)
    });
    
  }

  render() {
    return (
      <div className="login-form">
        {this.state.createSuccess
        ?
        <div class="alert alert-success" role="alert">
          A message was sent to your email.
        </div>
        :
        <p></p>
        }
        {this.state.createFail
        ?
        <div class="alert alert-danger" role="alert">
          This is a danger alert—check it out!
        </div>
        :
        <p></p>
        }
      <form>
        <h2 className="text-center">Log in</h2>       
        <div className="form-group">
          <input type="text" className="form-control" name="username" onChange={this.setParams} placeholder="Username" required="required" />
        </div>
        <div className="form-group">
          <input type="email" className="form-control" name="email" onChange={this.setParams} placeholder="Email" required="required" />
        </div>
        <div className="form-group">
          <input type="password" className="form-control" name="password" onChange={this.setParams} placeholder="Password" required="required" />
        </div>
        <div className="form-group">
          <input type="password" className="form-control" name="confirm_password" onChange={this.setParams} placeholder="Confirm password" required="required" />
        </div>
        <div className="form-group">
          <button type="button" className="btn btn-primary btn-block" onClick={this.handleSubmit}>Log in</button>
        </div>
        <div className="clearfix">
          <label className="float-left form-check-label"><input type="checkbox" /> Remember me</label>
          <Link to="/" className="float-right">Forgot Password?</Link>
        </div>        
      </form>
      <p className="text-center"><Link to="/">Create an Account</Link></p>
    </div>
    );
  }
}

export default SignupForm;